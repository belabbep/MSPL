---
title: "Compte rendu TP5"
author: "Pierre Belabbes"
date: "February 12, 2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}

library(readr);
   df <- read_tsv("dpt2016.txt", locale = locale(encoding = "ISO-8859-1"));
library(dplyr);
library(magrittr);
library(ggplot2);
```


```{r}


name_evolv_10 <- df %>% filter(preusuel =="PIERRE",annais != "XXXX") %>% group_by(annais) %>% summarize(nb = sum(nombre)) %>% mutate(annee = as.integer(annais)) %>% select(-annais) %>% group_by(annee_10 = annee - annee%%10) %>% summarize(avg = mean(nb)); 

ggplot(data=name_evolv_10, aes(x=annee_10, y=avg)) +
  geom_point() +
  geom_line() +
  xlab("Years") +
  ylab("Averages of birth of 'Pierre' between 1900 and 2016 in France")+
  theme_minimal();


```
The first name "Pierre" evolving along time since 1900. With the average each ten years.


```{r}
name_by_dpt_2016 <- df %>% filter( preusuel =="PIERRE", dpt != "XX", annais == "2016") %>% group_by(dpt) %>% summarize(nb = sum(nombre)) %>% arrange(desc(nb)) %>% head(20);

ggplot(data=name_by_dpt_2016, aes(x=dpt, y=nb)) +
  geom_bar(stat="identity",fill="steelblue")+
  xlab("Top 20 of departements of France of newborns named Pierre in 2016") +
  ylab("number of newborns")+
  geom_text(aes(label=nb), vjust=1.6, color="white", size=3.5)+
  theme_minimal();

```
Number of newborns named Pierre in each departement of France in 2016
